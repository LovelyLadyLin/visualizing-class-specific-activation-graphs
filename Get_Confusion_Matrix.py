# source https://stackoverflow.com/questions/53351963/mnist-get-confusion-matrix
# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html

import numpy as np
import tensorflow as tf
from keras import backend as K, metrics
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from sklearn.metrics import classification_report, confusion_matrix
from keras.models import Sequential, load_model
from keras.datasets import mnist
import Helpers


if __name__ == '__main__':

    # load trained model
    model = load_model('./mnist/act/mnist-model.h5')
    #
    # loading (preprocessed) dataset
    X_train, y_train = None, None
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    n_classes = 10
    n_inputs = 784
    # one-hot encoding using keras' numpy-related utilities
    Y_train = np_utils.to_categorical(y_train, n_classes)
    Y_test = np_utils.to_categorical(y_test, n_classes)

    # building the input vector from the 28x28 pixels
    X_train = X_train.reshape(60000, n_inputs)
    X_test = X_test.reshape(10000, n_inputs)
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')

    # normalizing the data to help with the training
    X_train /= 255
    X_test /= 255

    validation_data = (X_test, Y_test)
    validation_generator = validation_data

    batch_size = 128
    num_of_test_samples = len(X_test)

    test_predictions = model.predict(X_test)
    confusion = confusion_matrix(y_test, np.argmax(test_predictions, axis=1))

    print(type(confusion))
    print(confusion)
    total = 0
    right_classified = 0
    for i in range (10):
        sum_conf = sum(confusion[i])
        right_classified += confusion[i][i]
        print(sum_conf)
        total += sum_conf
    print("total num examples: ", total)
    print("right classified: ", right_classified)

    accuracy_base = model.evaluate(X_test, Y_test)
    print('The accuracy is ', accuracy_base)

    # Helpers.print_matrix_as_tex_tabel(confusion)
