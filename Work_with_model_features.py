import argparse
from io import BufferedWriter

from tensorflow.python.summary.writer.writer import FileWriter

import datetime
import os
import sys
import logging
import pickle
import gzip
from functools import partial

import numpy as np
import keras
from keras import backend as K
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation
from keras.datasets import mnist, fashion_mnist
from keras.utils import np_utils

import h5py as h5
import csv
import Helpers

try:
    from time import perf_counter
except:
    from time import time

    perf_counter = time

file_dst = "./data/"
delimiter = ','
print_labels_till_i = 0;        # need to be an integer
calc_avg_num = True;
get_activations = False;
get_weights_and_biases = False;
save_property = False;
calculate_avg_number = True;

def load_gzip(path, **kwargs):
    with gzip.open(path, 'rb') as fp:
        return pickle.load(fp, **kwargs)


def save_matrix_in_file(file_name, matrix, path=file_dst):

    file_directory = path + file_name
    with open(file_directory, 'w', newline='\n') as csvfile:
        file = csv.writer(csvfile, delimiter=delimiter,
                          quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for j in range(0, len(matrix)):
            file.writerow(matrix[j])


def save_array_in_file(file_name, array, path=file_dst):

    file_directory = path + file_name
    with open(file_directory, 'w', newline='\n') as csvfile:
        file = csv.writer(csvfile, delimiter=delimiter,
                          quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(array)

def sort_nodes_ascending(array, count_nodes_per_class, space_factor):
    num_negative = count_nodes_per_class[-1]
    order_pair = []
    for n in range(len(array)):
        order_pair.append([array[n], n])
    order_pair.sort()

    print("Order Pair_ascending : ", order_pair)
    print(type(order_pair))

    order = []
    for p in order_pair:
        order.append(p[1])

    #Append -1 nodes to end of array
    final_beginning = order[num_negative:]
    final_end = order[:num_negative]
    final = final_beginning + final_end

    final_sorting_pair = []
    class_c = 0
    next_step = count_nodes_per_class[class_c]
    for n in range(len(final)):
        #this is to add an empty space between nodes of distinct classes within each layer
        if(n == next_step):
            class_c += 1
            next_step += count_nodes_per_class[class_c]
        final_sorting_pair.append([final[n], n + class_c*space_factor])

    print('final_sorting_pair: \t', final_sorting_pair)
    final_sorting_pair.sort()

    final_sorting_single = []
    for p in final_sorting_pair:
        final_sorting_single.append(p[1])

    return final_sorting_single

def sort_nodes_new_order(array, count_nodes_per_class, space_factor, new_order):
    # the new order is based on the nodes two average input images have in common (see thesis)
    order_pair = []
    for n in range(len(array)):
        order_pair.append([array[n], n])

    order_pair_2 = sorted(order_pair, key=lambda x: new_order.index(x[0]))

    count_nodes_per_class_new_oder = [0] * 11
    for i in range(len(count_nodes_per_class)):
        index = new_order[i]
        count_nodes_per_class_new_oder[i] = count_nodes_per_class[index]

    order = []
    for p in order_pair_2:
        order.append(p[1])

    final_sorting_pair = []
    class_c = 0
    next_step = count_nodes_per_class_new_oder[class_c]

    for n in range(len(order)):
        #this is to add an empty space between nodes of distinct classes within each layer
        if(n == next_step):
            class_c += 1
            next_step += count_nodes_per_class_new_oder[class_c]
        final_sorting_pair.append([order[n], n + class_c*space_factor])
    print('final_sorting_pair: \t', final_sorting_pair)
    final_sorting_pair.sort()

    final_sorting_single = []
    for p in final_sorting_pair:
        final_sorting_single.append(p[1])

    return final_sorting_single

 # final 3D matrix for sorting with [c x #hidden layer x chosen nodes]
# eg #hidden layer = 2; num_branching = 2 -> [[[33, 50], [61, 103, 7, 61]], ..
def sort_nodes_bottom_up_vis_greedy(matrix_for_sorting, num_nodes_per_layer, new_order):
    final_order = []
    # for all hidden layers
    for h in range(len(matrix_for_sorting[0])):
        num_nodes_h = num_nodes_per_layer[-2 - h]
        # for all nodes within hidden layer h
        nodes_important_to_class = [-1] * num_nodes_h
        for n in range(num_nodes_h):
            # Greedy algorithm, because 0 gets all first, then 1, then 2 ...
            # Todo. sort greedy by importance value, not only color
            for c in reversed(range(len(matrix_for_sorting))):
                for curr_entry in range(len(matrix_for_sorting[0][h])):
                    if(matrix_for_sorting[c][h][curr_entry] == n):
                        nodes_important_to_class[n] = c
        # print(nodes_important_to_class)

        count = [0] * 11
        for i in nodes_important_to_class:
            if (i >= 0):
                count[i] += 1
            else:
                count[10] += 1
        sorted_order = sort_nodes_new_order(nodes_important_to_class, count, 6-(h*5), new_order)
        final_order = sorted_order + final_order

    return final_order

 # final 3D matrix for sorting with [c x #hidden layer x chosen nodes]
# eg #hidden layer = 2; num_branching = 2 -> [[[33, 50], [61, 103, 7, 61]], ..
def sort_nodes_bottom_up_vis_due_to_importance(matrix_for_sorting_importance, num_nodes_per_layer, new_order):
    final_order = []
    count_matrix = []
    # for all hidden layers
    for h in range(len(matrix_for_sorting_importance[0])):
        num_nodes_h = num_nodes_per_layer[-2 - h]
        # for all nodes within hidden layer h
        nodes_important_to_class = [-1] * num_nodes_h
        triple_in_hidden = []

        # make array of all triples [imp, node, class] and sort by importance
        for c in range (len(matrix_for_sorting_importance)):
            for b in range (len(matrix_for_sorting_importance[c][h])):
                triple_in_hidden.append(matrix_for_sorting_importance[c][h][b])
        triple_in_hidden.sort(reverse=True)

        # if(num_nodes_h != len(triple_in_hidden)): print("here is a misunderstanding")
        # print("num_nodes_h: ", num_nodes_h);
        # print("len(triple_in_hidden): ", len(triple_in_hidden))
        # print("triple_in_hidden: ", triple_in_hidden)

        for n in range(len(triple_in_hidden)):
            # decide which node belongs to which class
            current_node = triple_in_hidden[n][1]
            if(nodes_important_to_class[current_node] == -1):
                nodes_important_to_class[current_node] = triple_in_hidden[n][2]

        count = [0] * 11
        for i in nodes_important_to_class:
            if (i >= 0):
                count[i] += 1
            else:
                count[10] += 1
        count_matrix = [count] + count_matrix
        print(count_matrix)
        sorted_order = sort_nodes_new_order(nodes_important_to_class, count, 6-(h*5), new_order)
        final_order = sorted_order + final_order

    # save count to file
    file_name_count = "Bottom_Up_Total/Importance_bottom_up_count_surplus_b_" + str(num_branching)
    save_matrix_in_file(file_name_count, count_matrix)

    return final_order


if __name__ == '__main__':

    array_outpath = os.path.join('./mnist/act/mnist-model_info_acc.npy')
    statistics_array_acc = np.load(array_outpath)

    array_outpath = os.path.join('./mnist/act/mnist-model_info_delete.npy')
    statistics_array_delete = np.load(array_outpath)

    # load trained model
    model = load_model('./mnist/act/mnist-model.h5')
    model.summary()
    # get weights and biases of the model
    nn_property = model.get_weights()
    # extracting weights only
    weights = []
    for i in range(0, len(nn_property), 2):
        weights.append(nn_property[i])

    # save for java project in csv (comma-separated values) text format
    # save weights
    print(len(weights))
    file_name_base = "outgoing_weights_layer"
    for i in range(0, len(weights), 1):
        file_name = file_name_base + str(i)
        save_matrix_in_file(file_name, weights[i])

    # save statistics_array about accuracy
    save_matrix_in_file('statistics_acc', statistics_array_acc)

    # save statistics_array about deleted nodes
    save_matrix_in_file('statistics_del', statistics_array_delete)

    # save facts about the network:
    num_nodes_per_layer = []
    for i in range(0, len(weights)):
        num_nodes_per_layer.append(len(weights[i]))
    num_nodes_per_layer.append(len(weights[len(weights)-1][0]))

    print(num_nodes_per_layer, 'num_nodes_per_layer')

    save_array_in_file('num_nodes_per_layer', num_nodes_per_layer)


    # calculate (avg_act * weights + bias) to calculate most prominent path per class

    # load avg_activations of nodes per class
    data_outpath = './mnist/act/mnist-avg_acts.pklz'
    # avg_acts is of type list and has the dimensions: layers (4) x classes (10) x nodes_per_layer (784, ~150,  ~50,  10)
    avg_activations = load_gzip(data_outpath)

    # weights and biases are stored in nn_property = model.get_weights(), which is of type list and
    # is of form: [[weights_input], [biases_h1], [weights_h1], [biases_h2], [weights_h2], [biases_output]]
    # [weights] = array(input x output), [biases] = array(input)
    print('calculating avg_path ...')
    time_a = datetime.datetime.now()

    ################################################################################################################################################

    Visualizations = ["abs_number", "perc_of_total", "abs_number_bottom_up", "perc_bottom_up", "calc_sorting_order"]
    visualization = Visualizations[2]
    sorting_max_greedy = False      #type of sorting due to importance: T = max importance over all classes; F = each class same num
    sorting_vis = False              #additional sorting for bottom up visualizations based on the visualization
    num_branching = 1
    ascending_order = True

    ################################################################################################################################################

    # theta is an absolut number
    if(visualization == "abs_number"):
        max_perc_important_weights = 0.5
        # is of form: weight (3) x classes (10) x nodes_input (784, ~150,  ~50) x nodes_output (~150,  ~50,  10)]
        # e.g. print(avg_path[1][0][0]) = [1.3854767269885355, 942(input node), 970(output node)]
        avg_path = [] #3
        passed_nodes_in = 0
        passed_nodes_out = 0
        file_name_base = "important_" + str(max_perc_important_weights) + "perc_connections_"
        list_num_important_weights = []

        for l in range(len(avg_activations) - 1):
            avg_path_class = [] #10
            avg_path_class_reshape_for_file = []
            passed_nodes_out += len(weights[l])
            for c in range(len(avg_activations[0])):
                avg_path_layer_in = [] #num_nodes_in_layer_l
                avg_path_layer_in_reshape_for_file = []
                for n in range(len(avg_activations[l][0])):
                    # following line & + 6 lines needed if weights should be stored as matrix
                    #avg_path_weights = []   #num_nodes_in_layer_l+1
                    for o in range(len(avg_activations[l + 1][0])):
                        # calculate importance by activation * weights [+ bias](if node != input_node)
                        importance = avg_activations[l][c][n] * weights[l][n][o]
                        # leave bias, since equal to all nodes
                        # if(l > 0):
                        #    #add bias
                        #    importance += nn_property[(2*l)-1][n]
                        avg_path_layer_in.append([importance, n + passed_nodes_in, o + passed_nodes_out])
                    #avg_path_layer_in.append(avg_path_weights)
                avg_path_layer_in.sort(reverse=True)
                if(max_perc_important_weights > 0.2):
                    # cut list of important nodes to a specific length
                    num_important_weights = int(len(avg_path_layer_in) * max_perc_important_weights / 100)
                    avg_path_layer_in = avg_path_layer_in[:num_important_weights]
                avg_path_class.append(avg_path_layer_in)
                # concat. matrix [[v, n, o] ... [v, n, o]] to array [v, n, o ... v, n, o]
                avg_path_layer_in_reshape_for_file = np.concatenate(avg_path_layer_in)
                avg_path_class_reshape_for_file.append(avg_path_layer_in_reshape_for_file)
            list_num_important_weights.append(num_important_weights)
            passed_nodes_in += len(weights[l])
            avg_path.append(avg_path_class)

            # safe to file
            file_name = file_name_base + "layer_" + str(l)
            save_matrix_in_file(file_name, avg_path_class_reshape_for_file)


        time_d = datetime.datetime.now() - time_a
        print('process finished in ', time_d.seconds, '.', time_d.microseconds, ' seconds')

        # save statistics_array about deleted nodes
        file_name = 'num_important_weights_' + str(max_perc_important_weights) +  'perc'
        save_array_in_file(file_name, list_num_important_weights)


    # theta is a percentage of the total value
    elif(visualization == "perc_of_total"):
        max_perc_importance = 2
        # is of form: weight (3) x classes (10) x nodes_input (784, ~150,  ~50) x nodes_output (~150,  ~50,  10)]
        # e.g. print(avg_path[1][0][0]) = [1.3854767269885355, 942(input node), 970(output node)]
        avg_path = []  # 3
        passed_nodes_in = 0
        passed_nodes_out = 0
        file_name_base = 'important_activation_' + str(max_perc_importance) + 'perc_'
        list_num_important_weights = []

        for l in range(len(avg_activations) - 1):
            avg_path_class = []  # 10
            avg_path_class_reshape_for_file = []
            passed_nodes_out += len(weights[l])
            num_important_weights_helper = 0
            for c in range(len(avg_activations[0])):
                avg_path_layer_in = []  # num_nodes_in_layer_l
                avg_path_layer_in_reshape_for_file = []
                importance_total_layer_pos = 0
                for n in range(len(avg_activations[l][0])):
                    # following line & + 6 lines needed if weights should be stored as matrix
                    # avg_path_weights = []   #num_nodes_in_layer_l+1
                    for o in range(len(avg_activations[l + 1][0])):
                        # calculate importance by activation * weights [+ bias](if node != input_node)
                        importance = avg_activations[l][c][n] * weights[l][n][o]
                        if (l > 0):
                            importance += nn_property[(2 * l) - 1][n]
                        avg_path_layer_in.append([importance, n + passed_nodes_in, o + passed_nodes_out])
                        if(importance > 0):
                            importance_total_layer_pos += importance
                    # avg_path_layer_in.append(avg_path_weights)
                avg_path_layer_in.sort(reverse=True)
                if (max_perc_importance > 0.1):
                    # cut list of important nodes to a specific length
                    max_importance = importance_total_layer_pos * (max_perc_importance)/100
                    print("-----new class-----")
                    num_important_weights = 0
                    importance_value = avg_path_layer_in[0][0]
                    while(importance_value < max_importance):
                        num_important_weights += 1
                        importance_value += avg_path_layer_in[num_important_weights][0]
                    num_important_weights += 1
                    print("num_important_weights", num_important_weights)
                    avg_path_layer_in = avg_path_layer_in[:num_important_weights]
                    # if(num_important_weights > 0):
                    #     avg_path_layer_in = avg_path_layer_in[:num_important_weights]
                    # else:
                    #     avg_path_layer_in = []
                avg_path_class.append(avg_path_layer_in)
                # concat. matrix [[v, n, o] ... [v, n, o]] to array [v, n, o ... v, n, o]
                avg_path_layer_in_reshape_for_file = np.concatenate(avg_path_layer_in)
                avg_path_class_reshape_for_file.append(avg_path_layer_in_reshape_for_file)
                print("num_important_weights layer ", l , ": ", num_important_weights)
                if(num_important_weights > num_important_weights_helper):
                    num_important_weights_helper = num_important_weights
            list_num_important_weights.append(num_important_weights_helper)
            passed_nodes_in += len(weights[l])
            avg_path.append(avg_path_class)

        # safe to file
        file_name = file_name_base + "layer_" + str(l)
        save_matrix_in_file(file_name, avg_path_class_reshape_for_file)

        time_d = datetime.datetime.now() - time_a
        print('process finished in ', time_d.seconds, '.', time_d.microseconds, ' seconds')

        # save statistics_array about deleted nodes
        file_name = 'important_activation_' + str(max_perc_importance) + 'perc_num'
        save_array_in_file(file_name, list_num_important_weights)

    # Visualizing a bottom_up structure
    elif(visualization == "abs_number_bottom_up"):
        passed_nodes = [0]
        for i in range(len(num_nodes_per_layer) - 1):
            passed_nodes.append(passed_nodes[i] + num_nodes_per_layer[i])
        print('passed_nodes: ', passed_nodes)

        # final matrix with [classes x paths]
        paths_all = []

        # final 3D matrix for sorting with [c x #hidden layer x chosen nodes]
        # eg #hidden layer = 2; num_branching = 2 -> [[[33, 50], [61, 103, 7, 61]], [[15, 36], [86, 63, 1, 115]], ...]]]
        matrix_for_sorting = []
        matrix_for_sorting_importance = []

        for c in range(0, 10, 1):
            #total path in the end
            path_class = []
            path_class_2 = []
            array_for_sorting = []
            array_for_sorting_importance = []

            #active nodes of former layer
            out_nodes = [c]
            out_nodes_importance = [c]
            out_nodes_help = []
            out_nodes_importance_help = []

            #counting 2, 1, 0
            for l in reversed(range(len(num_nodes_per_layer) - 1)):
                importance_array_layer = []

                # append all chosen nodes of the hidden layers only to the array_for_sorting
                if(out_nodes != [c]):
                    array_for_sorting.append(out_nodes)
                    array_for_sorting_importance.append(out_nodes_importance)

                for o in out_nodes:
                    importance_to_specific_out = []
                    for n in range(num_nodes_per_layer[l]):
                        # calculate importance by activation * weights [+ bias](if node != input_node)
                        importance = avg_activations[l][c][n] * weights[l][n][o]
                        importance_to_specific_out.append([importance, n, o])

                    #get num_nodes most important connections
                    importance_to_specific_out.sort(reverse=True)
                    importance_to_specific_out = importance_to_specific_out[:num_branching]


                    for k in range(len(importance_to_specific_out)):
                        out_nodes_help.append(importance_to_specific_out[k][1])
                        out_nodes_importance_help.append([importance_to_specific_out[k][0], importance_to_specific_out[k][1], c])
                        importance_to_specific_out[k][1] += passed_nodes[l]
                        importance_to_specific_out[k][2] += passed_nodes[l+1]

                        path_class.append(importance_to_specific_out[k][0])
                        path_class.append(importance_to_specific_out[k][1])
                        path_class.append(importance_to_specific_out[k][2])

                out_nodes = out_nodes_help
                out_nodes_importance = out_nodes_importance_help
                out_nodes_help = []
                out_nodes_importance_help = []

            # print(path_class)
            paths_all.append(path_class)
            matrix_for_sorting.append(array_for_sorting)
            matrix_for_sorting_importance.append(array_for_sorting_importance)
        # print(paths_all)
        # print('matrix_for_sorting: ', matrix_for_sorting)

        # safe to file
        file_name = "Bottom_Up_Total/Importance_bottom_up_branching_" + str(num_branching)
        save_matrix_in_file(file_name, paths_all)

        time_d = datetime.datetime.now() - time_a
        print('process finished in ', time_d.seconds, '.', time_d.microseconds, ' seconds')

        if(sorting_vis):
            if(ascending_order):
                new_order = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, -1]
                file_name = "Bottom_Up_Total/Ordering_hidden_nodes_bottom_up_vis_greedy_ascending_branching_" + str(num_branching)
            else:
                new_order = [0, 3, 5, 8, 2, 6, 4, 9, 7, 1, -1]
                file_name = "Bottom_Up_Total/Ordering_hidden_nodes_bottom_up_vis_greedy_neworder_branching_"+ str(num_branching)

            # safe to file
            # final_order = sort_nodes_bottom_up_vis_greedy(matrix_for_sorting, num_nodes_per_layer, new_order)
            # print("final_order_gre: ", final_order)
            final_order = sort_nodes_bottom_up_vis_due_to_importance(matrix_for_sorting_importance, num_nodes_per_layer, new_order)
            print("final_order_imp: ", final_order)
            save_array_in_file(file_name, final_order)

    elif (visualization == "perc_bottom_up"):
        for i in range (1, 15):
            perc_importance_branching = i
            passed_nodes = [0]
            for i in range(len(num_nodes_per_layer) - 1):
                passed_nodes.append(passed_nodes[i] + num_nodes_per_layer[i])
            print(passed_nodes)

            paths_all = []

            for c in range(0, 10, 1):
                # total path in the end
                path_class = []
                path_class_2 = []

                # active nodes of former layer
                out_nodes = [c]
                out_nodes_help = []

                # counting 2, 1, 0
                for l in reversed(range(len(num_nodes_per_layer) - 1)):
                    importance_array_layer = []

                    for o in out_nodes:
                        sum_importance = 0
                        importance_to_specific_out = []
                        for n in range(num_nodes_per_layer[l]):
                            # calculate importance by activation * weights [+ bias](if node != input_node)
                            importance = avg_activations[l][c][n] * weights[l][n][o]
                            importance_to_specific_out.append([importance, n, o])
                            sum_importance += importance

                        # find number of weights such that their sum =< perc_importance_branching of total
                        importance_to_specific_out.sort(reverse=True)

                        if (perc_importance_branching > 0.05):
                            # cut list of important nodes to a specific length
                            max_importance = sum_importance * (perc_importance_branching) / 100
                            num_important_weights = 0
                            importance_value = 0
                            while (importance_value < max_importance):
                                num_important_weights += 1
                                importance_value += importance_to_specific_out[num_important_weights][0]
                            if (num_important_weights == 0):
                                num_important_weights += 1

                            #print("num_important_weights", num_important_weights)
                            importance_to_specific_out = importance_to_specific_out[:num_important_weights]

                        # generate array
                        for k in range(len(importance_to_specific_out)):
                            out_nodes_help.append(importance_to_specific_out[k][1])
                            importance_to_specific_out[k][1] += passed_nodes[l]
                            importance_to_specific_out[k][2] += passed_nodes[l + 1]

                            path_class.append(importance_to_specific_out[k][0])
                            path_class.append(importance_to_specific_out[k][1])
                            path_class.append(importance_to_specific_out[k][2])

                    out_nodes = out_nodes_help
                    out_nodes_help = []

                print('path_class: ', path_class)
                paths_all.append(path_class)
            print('paths_all: ', paths_all)

            max_num_entries = 0
            for i in range(len(paths_all)):
                if(max_num_entries < len(paths_all[i])):
                    max_num_entries = len(paths_all[i])

            # safe to file
            file_name = "Bottom_Up_Perc/Importance_bottom_up_branching_perc_" + str(perc_importance_branching)
            save_matrix_in_file(file_name, paths_all)

            # safe to file
            file_name = "Bottom_Up_Perc/Importance_bottom_up_branching_perc_max_num_entries" + str(perc_importance_branching)
            save_array_in_file(file_name, [max_num_entries])

            time_d = datetime.datetime.now() - time_a
            print('process finished in ', time_d.seconds, '.', time_d.microseconds, ' seconds')

    elif(visualization == "calc_sorting_order"):
        passed_nodes_in = 0
        passed_nodes_out = len(avg_activations[0][0])

        #Matrix to store all importance values for all 10 input classes for each node
        num_hidden_nodes = np.sum(num_nodes_per_layer) - num_nodes_per_layer[0] - num_nodes_per_layer[len(num_nodes_per_layer)-1]
        total_importance = np.zeros((10, num_hidden_nodes))

        for c in range(10):
            passed_nodes_in = 0
            passed_nodes_out = num_nodes_per_layer[1]
            for l in range(1, len(num_nodes_per_layer) - 2):
                for n in range(num_nodes_per_layer[l]):
                    for o in range(num_nodes_per_layer[l+1]):
                        # calculate importance by activation * weights [+ bias](if node != input_node)
                        # Todo: Because there are all negative values included to in and output of the combination,
                        #  there are so many nodes in the category 'rest' in the visualizations
                        total_importance[c][n + passed_nodes_in] += avg_activations[l][c][n] * weights[l][n][o]
                        total_importance[c][o + passed_nodes_out] += avg_activations[l][c][n] * weights[l][n][o]

                passed_nodes_in = passed_nodes_out
                passed_nodes_out += num_nodes_per_layer[l+1]

        time_d = datetime.datetime.now() - time_a
        print('process finished in ', time_d.seconds, '.', time_d.microseconds, ' seconds')

        if (sorting_max_greedy):
            file_name = "Ordering_Importance/Ordering_hidden_nodes_greedy"
            file_name_statistics = "Ordering_Importance/Ordering_count_greedy"
            # matrix to store which node is most important for which class
            nodes_important_to_class = []
            for n in range(num_hidden_nodes):
                max_importance = 0
                belonging_class = -1
                for c in range(10):
                    if(max_importance < total_importance[c][n]):
                        max_importance = total_importance[c][n]
                        belonging_class = c
                nodes_important_to_class.append(belonging_class)

            hidden_1 = nodes_important_to_class[:num_nodes_per_layer[1]]
            hidden_2 = nodes_important_to_class[num_nodes_per_layer[1]:]

        else:
            file_name = "Ordering_Importance/Ordering_hidden_nodes_equal"
            file_name_statistics = "Ordering_Importance/Ordering_count_equal"
            hidden_1 = [-1] * num_nodes_per_layer[1]
            hidden_2 = [-1] * num_nodes_per_layer[2]
            # all nodes need to be assigned to a class
            for n in range(num_nodes_per_layer[1]):
                # each class after the other 0-1-2-...9-0-1-2-...
                class_curr = n % 10
                max_node = -1
                max_val = 0
                for curr_node in range(num_nodes_per_layer[1]):
                    if(max_val < total_importance[class_curr][curr_node]):
                        max_val = total_importance[class_curr][curr_node]
                        max_node = curr_node

                for i in range(10):
                    total_importance[i][max_node] = -1

                if(max_val >= 0):
                    hidden_1[max_node] = class_curr

            for n in range(num_nodes_per_layer[2]):
                # each class after the other 0-1-2-...9-0-1-2-...
                class_curr = n % 10
                max_node = -1
                max_val = 0
                passed_nodes = num_nodes_per_layer[1]
                for curr_node in range(passed_nodes, num_nodes_per_layer[2] + passed_nodes, 1):
                    if(max_val < total_importance[class_curr][curr_node]):
                        max_val = total_importance[class_curr][curr_node]
                        max_node = curr_node

                for i in range(10):
                    total_importance[i][max_node] = -1

                if(max_val > 0):
                    hidden_2[max_node - passed_nodes] = class_curr

        ### generate order for nodes
        # how many nodes are important for each class
        statistic = []
        count = [0] * 11
        for i in hidden_1:
            if (i >= 0):
                count[i] += 1
            else:
                count[10] += 1
        statistic.append(count)
        order_1 = sort_nodes_ascending(hidden_1, count, 1)

        count = [0] * 11
        for i in hidden_2:
            if (i >= 0):
                count[i] += 1
            else:
                count[10] += 1
        statistic.append(count)
        order_2 = sort_nodes_ascending(hidden_2, count, 5)

        #safe statistics
        print('statistic: \t\t', statistic)

        order_final = order_1 + order_2
        print('order_final: \t', order_final)

        # safe to file
        save_array_in_file(file_name, order_final)

        # safe to file
        save_matrix_in_file(file_name_statistics, statistic)


