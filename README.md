# Visualizing Class-specific Activation Graphs

This repository contains the material developed for my master's thesis with the title 'Understanding neural networks decisions by visualizing class-specific activation graphs'.  For more details on the research see the thesis itself. 